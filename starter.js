const app = "app";

/**
 * function configuration variable public
 */
module.exports.public = () => {

  /**
   * create variable global
   */
  global.g = require(`./${app}/utils/public`);
  global.config = require(`./${app}/config`);
  global.h = require(`./${app}/helpers`);
  global.r = require(`./${app}/response`);
  global.m = require(`./${app}/middleware`);
  global.c = require(`./${app}/controllers`);

  /**
   * test connection database parpol
   */
  // let db = require("./database/parpol/models");

  /**
   * set variable database to global
   */
  // global.db = db;

  /**
   * starting cron job
   */
  require("./cron");
};

/**
 * function configuration variable locals for access scope web
 * @param {res} res | response
 */
module.exports.locals = (res) => {
  Object.assign(res.locals, require(`./${app}/utils/locals`));
};