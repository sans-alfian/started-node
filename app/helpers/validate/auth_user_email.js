
module.exports = async (value, unique = true) => {
  if (!value) {
    return "email is required";
  }

  if (unique) {
    let user_by_email = await db.auth_account.findAll(
      {
        limit: 1,
        where: {
          email: value
        },
        order: [['id', 'DESC']]
      }
    );
    if (user_by_email.length != 0) {
      return "email has been used";
    }
  }
}