/**
 * function returns string data "Hello name"
 * @param {name} name from user or endpoint
 * @returns Say Hallo "Hello name"
 */
module.exports = (name) => {
  /**
   * return string data "Hello Name"
   */
  return `Hello ${name}`;
};