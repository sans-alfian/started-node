const jwt = require("jsonwebtoken");

/**
 * middleware to check token
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next 
 * @returns return matching middleware
 */
module.exports = async (req, res, next) => {

  // block try
  try {

    // get token from request headers access token
    let token = req.headers.authorization;

    // isEmpty token
    if (!token) {
      return r.errorNotAuth(res, "token not found and token is required")
    }

    token = token.split(' ')[1]

    // isEmpty token
    if (!token) {
      return r.errorNotAuth(res, "token not found and token is required")
    }

    // declaration variable user
    let user = {};

    // try get user with token
    try {
      user = jwt.verify(token, g.secret_key);
    }

    // handle error jwt
    catch (err) {

      // error expired token
      if (err instanceof jwt.TokenExpiredError) {

        // send response with expired token
        return r.errorNotFound(res, "token has expired and please update now")
      }

      // token invalid
      if (err.message == "invalid signature") {

        // send response with invalid token
        return r.errorNotFound(res, "invalid token and please update now")
      }

      // token invalid
      if (err.message == "jwt malformed") {

        // send response with invalid token
        return r.errorNotFound(res, "invalid token and please update now")
      }

      // token invalid
      if (err.message == "invalid token") {

        // send response with invalid token
        return r.errorNotFound(res, "invalid token and please update now")
      }

      // send response with error code
      return r.errorServer(res, err);
    }

    let dataToken = await db.auth_token.findOne(
      {
        where: {
          token
        }
      }
    )

    if (!dataToken) {
      // send response with expired token
      return r.errorNotFound(res, "token has expired and please update now")
    }

    if (req.use_verified) {
      let account = await db.auth_account.findByPk(user.id)
      if (!account.verified) {
        return r.errorNotAuth(res, "account not verified")
      }
    }

    req.user = user
    return next()
  }

  // handle error code
  catch (err) {
    return r.errorServer(res, err);
  }
};