
module.exports = (res, message = "Not Allowed To Access", data = {}) => {
  return res.status(g.code_http.UNAUTHORIZED).json({
    success: false,
    code: g.code_http.UNAUTHORIZED,
    data,
    message
  }).end();
};