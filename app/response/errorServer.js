
module.exports = (res, err) => {
  console.log(err);
  return res.status(g.code_http.ERROR_SERVER).json({
    success: false,
    code: g.code_http.ERROR_SERVER,
    data: {},
    message: err.message
  }).end();
};