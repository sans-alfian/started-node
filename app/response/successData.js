module.exports = (res, data = {}, message = "") => {
  return res.status(g.code_http.SUCCESS).json({
    success: true,
    code: g.code_http.SUCCESS,
    data,
    message
  }).end();
};