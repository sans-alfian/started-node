
module.exports = (res, message = "Forbidden", data = {}) => {
  return res.status(g.code_http.FORBIDDEN).json({
    success: false,
    code: g.code_http.FORBIDDEN,
    data,
    message
  }).end();
};