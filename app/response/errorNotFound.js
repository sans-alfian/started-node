
module.exports = (res, message = "Request Not Found", data = {}) => {
  return res.status(g.code_http.NOT_FOUND).json({
    success: false,
    code: g.code_http.NOT_FOUND,
    data,
    message
  }).end();
};