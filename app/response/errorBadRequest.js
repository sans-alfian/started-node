
module.exports = (res, message = "Bad Request", data = {}) => {
  return res.status(g.code_http.BAD_REQUEST).json({
    success: false,
    code: g.code_http.BAD_REQUEST,
    data,
    message
  }).end();
};