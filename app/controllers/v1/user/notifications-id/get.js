require('dotenv').config();
const moment = require('moment');
const { Op } = require('sequelize');

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let notification = await db.auth_notification.findByPk(req.params.notification_id)

    if (!notification) {
      return r.errorNotFound(res, "not found")
    }

    notification = {
      id: notification.id,
      title: notification.title,
      message: notification.message,
      waktu: moment(new Date(notification.waktu)).format("YYYY-MM-DD HH:mm:ss"),
      icon: notification.icon,
      read: notification.read
    }

    /**
     * response data
     */
    const data = {
      notification
    };

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "notification user volunteer");
  } catch (err) {
    return r.errorServer(res, err);
  }

};