require('dotenv').config();
const moment = require('moment');
const { Op } = require('sequelize');

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let limit = 10
    if (req.query.limit) {
      limit = req.query.limit
    }

    let page = 1
    if (req.query.page) {
      page = req.query.page
    }

    let where = {
      account_id: req.user.id
    }

    if (req.body.start_date) {
      where.waktu = {
        [Op.gt]: new Date(req.query.start_date).setHours(0, 0, 0, 0)
      }
    }

    if (req.body.end_date) {
      where.waktu = {
        [Op.lt]: new Date(req.query.end_date).setHours(23, 59, 59, 0)
      }
    }

    let notifications = []
    let notification = await db.auth_notification.findAll(
      {
        where,
        limit,
        offset: (page - 1) * limit
      }
    )

    notification.forEach(n => {
      notifications.push(
        {
          id: n.id,
          title: n.title,
          message: n.message,
          waktu: moment(new Date(n.waktu)).format("YYYY-MM-DD HH:mm:ss"),
          icon: n.icon,
          read: n.read
        }
      )
    });

    /**
     * response data
     */
    const data = {
      notifications,
      total: notifications.length,
      limit,
      page
    };

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "list notifications user volunteer");
  } catch (err) {
    return r.errorServer(res, err);
  }

};