require('dotenv').config();
const moment = require('moment');
const { Op } = require('sequelize');

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let activity = await db.auth_activity.findByPk(req.params.activity_id)

    if (!activity) {
      return r.errorNotFound(res, "not found")
    }

    activity = {
      id: activity.id,
      name: activity.name,
      description: activity.description,
      waktu: moment(new Date(activity.waktu)).format("YYYY-MM-DD HH:mm:ss"),
      icon: activity.icon
    }

    /**
     * response data
     */
    const data = {
      activity
    };

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "activity user volunteer");
  } catch (err) {
    return r.errorServer(res, err);
  }

};