require('dotenv').config();

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let account = await db.auth_account.findByPk(req.user.id)

    await db.auth_user.update(
      {
        name: req.body.name
      },
      {
        where: {
          id: account.user_id
        }
      }
    )

    await db.auth_account.update(
      {
        email: req.body.email,
        phone_number: req.body.phone_number,
        image: req.body.image
      },
      {
        where: {
          id: account.id
        }
      }
    )
    /**
     * response data
     */
    const data = {};

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "profile changed successfully");
  } catch (err) {
    return r.errorServer(res, err);
  }

};