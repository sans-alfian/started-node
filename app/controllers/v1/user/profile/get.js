require('dotenv').config();

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let account = await db.auth_account.findByPk(req.user.id)

    let user = await db.auth_user.findByPk(account.user_id)
    let user_group = await db.auth_user_group_n.findOne(
      {
        where: {
          account_id: account.id,
          is_default: true
        }
      }
    )

    /**
     * response data
     */
    const data = {
      user: {
        id: account.id,
        name: user.name,
        email: account.email,
        phone_number: account.phone_number,
        image: user.profile
      },
      role: {
        id: user_group.group_id,
        name: user_group.group_name
      },
    };

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "profile user");
  } catch (err) {
    return r.errorServer(res, err);
  }

};