

module.exports = async (req, res, next) => {

  // block try
  try {

    let password = h.validate.auth_user_password(req.body.password);
    if (password) {
      return r.errorBadRequest(res, password);
    }

    let new_password = h.validate.auth_user_password(req.body.new_password);
    if (new_password) {
      return r.errorBadRequest(res, "new password is required");
    }

    return next()
  }

  // handle error code
  catch (err) {
    return r.errorServer(res, err);
  }
};