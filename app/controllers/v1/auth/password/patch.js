/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let account = await db.auth_account.findByPk(req.user.id)

    // compare password and password user
    if (!h.comparePassword(req.body.password, { id: account.id, password: account.password })) {

      // send response with error not found because password incorrect
      return r.errorBadRequest(res, "password doesn't match");
    }

    await db.auth_account.update(
      {
        password: h.sha(`${account.id}-${req.body.new_password}`)
      },
      {
        where: {
          id: account.id
        }
      }
    )

    let password = await db.auth_password.add(
      {
        account_id: account.id,
        password: req.body.new_password
      }
    )

    /**
     * response data
     */
    const data = {};

    /**
     * response success data
     */
    return r.successData(res, data, "password changed successfully");
  } catch (err) {
    return r.errorServer(res, err);
  }

};