

module.exports = async (req, res, next) => {

  // block try
  try {

    let auth_user_name = h.validate.auth_user_name(req.body.name);
    if (auth_user_name) {
      return r.errorBadRequest(res, auth_user_name);
    }

    let auth_user_email = await h.validate.auth_user_email(req.body.email);
    if (auth_user_email) {
      return r.errorBadRequest(res, auth_user_email);
    }

    let auth_user_phone_number = h.validate.auth_user_phone_number(req.body.phone_number);
    if (auth_user_phone_number) {
      return r.errorBadRequest(res, auth_user_phone_number);
    }

    let auth_user_password = h.validate.auth_user_password(req.body.password);
    if (auth_user_password) {
      return r.errorBadRequest(res, auth_user_password);
    }

    return next()
  }

  // handle error code
  catch (err) {
    return r.errorServer(res, err);
  }
};