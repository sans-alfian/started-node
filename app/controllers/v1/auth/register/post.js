const jwt = require("jsonwebtoken");

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let user = await db.auth_user.add(
      {
        name: req.body.name,
        profile: "default.png"
      }
    )

    let account = await db.auth_account.add(
      {
        user_id: user.id,
        email: req.body.email,
        phone_number: req.body.phone_number,
        password: req.body.password,
        verified: false,
        otp: 0
      }
    )

    let password = await db.auth_password.add(
      {
        account_id: account.id,
        password: req.body.password
      }
    )

    let user_group = await db.auth_user_group.add(
      {
        account_id: account.id,
        group_id: 6,
        is_default: true
      }
    )
    let group = user_group.group

    let data_jwt = {
      id: account.id
    }
    // create token jwt
    let token = jwt.sign(data_jwt, g.secret_key, {})

    // set token to database
    token = await db.auth_token.createToken(account.id, token)

    /**
     * response data
     */
    const data = {
      user: {
        id: account.id,
        name: user.name,
        email: account.email,
        phone_number: account.phone_number,
        image: user.profile
      },
      role: {
        id: group.id,
        name: group.name
      },
      token: `Bearer ${token}`
    }

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "register has been successfully")
  } catch (err) {
    return r.errorServer(res, err)
  }

}