/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let account = await db.auth_account.findOne(
      {
        where: {
          email: req.body.email
        }
      }
    )

    if (!account) {
      return r.errorNotFound(res, "e-mail is not registered")
    }


    if (!account.verified) {
      return r.errorNotAuth(res, "account not verified")
    }

    // actions
    await db.auth_account.update(
      {
        password: h.sha(`${account.id}-${req.body.new_password}`)
      },
      {
        where: {
          id: account.id
        }
      }
    )

    let password = await db.auth_password.add(
      {
        account_id: account.id,
        password: req.body.new_password
      }
    )

    /**
     * response data
     */
    const data = {};

    /**
     * response success data
     */
    return r.successData(res, data, "password changed successfully");
  } catch (err) {
    return r.errorServer(res, err);
  }

};