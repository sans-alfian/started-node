

module.exports = async (req, res, next) => {

  // block try
  try {

    let email = await h.validate.auth_user_email(req.body.email, false);
    if (email) {
      return r.errorBadRequest(res, email);
    }

    let new_password = h.validate.auth_user_password(req.body.new_password);
    if (new_password) {
      return r.errorBadRequest(res, "new password is required");
    }

    return next()
  }

  // handle error code
  catch (err) {
    return r.errorServer(res, err);
  }
};