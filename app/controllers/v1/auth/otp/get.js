require('dotenv').config();

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let account = await db.auth_account.findByPk(req.user.id)
    let otp = Math.floor(100000 + Math.random() * 900000);

    await db.auth_account.update(
      {
        otp
      },
      {
        where: {
          id: account.id
        }
      }
    )

    // h.mailer.sendMail({
    //   from: process.env.MAILER_FROM_EMAIL,
    //   to: account.email,
    //   subject: 'OTP',
    //   text: `Kode OTP ${otp}`
    // })

    /**
     * response data
     */
    const data = {};

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "sending otp code");
  } catch (err) {
    return r.errorServer(res, err);
  }

};