

module.exports = (req, res, next) => {

  // block try
  try {

    if (h.validate.auth_user_otp(req.body.otp)) {
      return r.errorBadRequest(res, h.validate.auth_user_otp(req.body.otp));
    }

    return next()
  }

  // handle error code
  catch (err) {
    return r.errorServer(res, err);
  }
};