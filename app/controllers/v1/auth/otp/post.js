/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let account = await db.auth_account.findByPk(req.user.id)

    if (req.body.otp != account.otp) {
      return r.errorBadRequest(res, "otp doesn't match");
    }

    await db.auth_account.update(
      {
        verified: true
      },
      {
        where: {
          id: account.id
        }
      }
    )

    /**
     * response data
     */
    const data = {};

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "successful verification");
  } catch (err) {
    return r.errorServer(res, err);
  }

};