const jwt = require("jsonwebtoken");

/**
 * endpoint send data
 * @param {request} request endpoint
 * @param {response} response endpoint
 * @param {next} next
 * @returns send response successData
 */
module.exports = async (req, res, next) => {

  /**
   * handle error response
   */
  try {

    let account = await db.auth_account.findOne(
      {
        where: {
          email: req.body.email
        }
      }
    )

    // compare password and password user
    if (!h.comparePassword(req.body.password, { id: account.id, password: account.password })) {

      // send response with error not found because password incorrect
      return r.errorBadRequest(res, "password doesn't match");
    }

    let user = await db.auth_user.findByPk(account.user_id)
    let user_group = await db.auth_user_group_n.findOne(
      {
        where: {
          account_id: account.id,
          is_default: true
        }
      }
    )

    let data_jwt = {
      id: account.id
    }
    // create token jwt
    let token = jwt.sign(data_jwt, g.secret_key, {})

    // set token to database
    token = await db.auth_token.createToken(account.id, token)

    /**
     * response data
     */
    const data = {
      user: {
        id: account.id,
        name: user.name,
        email: account.email,
        phone_number: account.phone_number,
        image: user.profile
      },
      role: {
        id: user_group.group_id,
        name: user_group.group_name
      },
      token: `Bearer ${token}`
    }

    /**
     * response success data after login successful
     */
    return r.successData(res, data, "login has been successful")
  } catch (err) {
    return r.errorServer(res, err)
  }

}