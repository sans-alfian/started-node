require('dotenv').config();
module.exports = {
  code_http: {
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404,
    SUCCESS: 200,
    ERROR_SERVER: 500
  },

  secret_key: '3ac6adca4a77ed7f393d885dd03d7ea9d1766304' // !P@ssw0rd

};