/**
 * Set Utils Public
 */
require('dotenv').config();
require('./starter').public();

/**
 * Module dependencies.
 */
const createError = require('http-errors');
const express = require('express');
const session = require('express-session');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const cors = require('cors');
const flash = require('express-flash');

/**
 * app
 */
const app = express();

/**
 * configuration views engine with ejs
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

/**
 * configuration session
 */
app.use(session({
  secret: 'secret',
  resave: true,
  saveUninitialized: true
}));

/**
 * logger
 */
app.use(logger('dev'));

/**
 * request limit and with json encoding
 */
app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({
  extended: true
}));

/**
 * middleware cors for manage routes
 */
app.use(cors());
app.use(flash());
app.use(cookieParser());

/**
 * create static page
 */
app.use(express.static(path.join(__dirname, 'public')));

/**
 * started with starter file
 */
app.use((req, res, next) => {
  require('./starter').locals(res);
  req.data = {};
  req.setting = {};
  if (req.user) {
    res.locals.user = req.user;
  }
  next();
});

/**
 * test database connection
 */
app.use(async (req, res, next) => {
  // try {
  //   if (!req.session.db) {
  //     await db.sequelize.authenticate();
  //     req.session.db = true
  //   }
  // } catch (error) {
  //   console.error('Unable to connect to the database:', error);
  //   return r.errorServer(res, error);
  // }
  next();
});

/**
 * use x-access-token
 */
// app.use(require("./app/middleware/api/useAccessToken"));

/**
 * routes
 */
global.__basedir = __dirname;
// app.use('/api', require('./routes/api'));
app.use('/', require('./routes/index'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;