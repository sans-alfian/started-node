const express = require('express');
const router = express.Router();


/**
 * endpoint all "/api"
 */
router.all('/', async (req, res, next) => {

  /**
   * send response with status not found
   */
  return r.errorNotFound(res);
});

/**
 * handle endpoint with folder
 */
config.routes(router, __filename, __dirname);

/**
 * handle endpoint with redirect to '/api
 */
router.all('/*', async (req, res) => res.redirect('/api'));

module.exports = router;