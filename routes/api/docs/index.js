const express = require('express');
const router = express.Router();
const swagger = require('swagger-ui-express');

const docs = [
  'administrator', 'dashboard', 'mobile'
]

for (let i = 0; i < docs.length; i++) {

  let file_docs = `${__basedir}/docs-api/${docs[i]}-docs-api-v1.0.0.json`;

  /**
   * Endpoint for download docs api documentation
   */
  router.get(`/${docs[i]}/download`, async (req, res) => {

    /**
     * directory file api documentation
     */
    let file = file_docs;
    res.download(file);
  });


  /**
   * Docs API with Swagger UI
   */
  let swaggerDocument = require(file_docs);
  router.use(`/${docs[i]}/`,
    (req, res, next) => {
      next()
    },
    swagger.serve,
    (req, res, next) => swagger.setup(swaggerDocument, {
      swaggerOptions: {
        displayOperationId: true,
        plugins: [

        ]
      }
    })(req, res, next)
  );

}

/**
 * endpoint GET "/api/docs"
 */
router.get("/",
  async (req, res, next) => res.redirect('/api/docs/administrator')
);


/**
 * handle endpoint with folder
 */
// config.routes(router, __filename, __dirname);


module.exports = router;