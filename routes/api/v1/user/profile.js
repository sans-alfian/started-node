const express = require('express');
const router = express.Router();

/**
 * endpoint GET "/user/profile"
 */
router.get("/",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.user.profile.get
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

/**
 * endpoint PUT "/user/profile"
 */
router.put("/",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.user.profile.put
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

module.exports = router;