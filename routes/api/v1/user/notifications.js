const express = require('express');
const router = express.Router();

/**
 * endpoint GET "/user/notifications"
 */
router.get("/",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.user.notifications.get
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

/**
 * endpoint GET "/user/notifications"
 */
router.get("/:notification_id",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.user["notifications-id"].get
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

module.exports = router;