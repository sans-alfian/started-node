const express = require('express');
const router = express.Router();

/**
 * endpoint GET "/user/activities"
 */
router.get("/",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.user.activities.get
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

/**
 * endpoint GET "/user/activities"
 */
router.get("/:activity_id",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.user["activities-id"].get
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

module.exports = router;