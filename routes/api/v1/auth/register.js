const express = require('express');
const router = express.Router();

/**
 * endpoint POST "/auth/register"
 */
router.post("/",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      c.v1.auth.register.post_validate,
      c.v1.auth.register.post
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

module.exports = router;