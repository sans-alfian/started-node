const express = require('express');
const router = express.Router();

/**
 * endpoint PATCH "/auth/password"
 */
router.patch("/",
  async (req, res, next) => {

    req.use_verified = true

    // set middleware
    let middleware = [
      m.x_access_token,
      c.v1.auth["forgot-password"].patch_validate,
      c.v1.auth["forgot-password"].patch
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

module.exports = router;