const express = require('express');
const router = express.Router();

/**
 * endpoint all "/api/v1/auth"
 */
router.all('/', async (req, res, next) => {

  /**
   * send response with status forbidden
   */
  return r.errorForbidden(res, "Rest API V1 --> Auth");
});

/**
 * handle endpoint with folder
 */
config.routes(router, __filename, __dirname);

/**
 * handle endpoint with redirect to '/api
 */
router.all('/*', async (req, res) => res.redirect('/api'));

module.exports = router;