const express = require('express');
const router = express.Router();

/**
 * endpoint GET "/auth/otp"
 */
router.get("/",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.auth.otp.get
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

/**
 * endpoint POST "/auth/otp"
 */
router.post("/",
  async (req, res, next) => {

    // set middleware
    let middleware = [
      m.x_access_token,
      m.token,
      c.v1.auth.otp.post_validate,
      c.v1.auth.otp.post
    ];

    // call middleware function custom
    h.runMiddleware(middleware, req, res);
  }
);

module.exports = router;