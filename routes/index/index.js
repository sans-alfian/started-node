const express = require('express');
const router = express.Router();

/**
 * endpoint GET "/"
 */
router.get('/', async (req, res, next) => {
  // console.log(a.a.a)
  /**
   * response sayHallo "sanjari";
   * "Hallo Sanjari"
   */
  return r.successData(res, {}, h.sayHello("Sanjari"));
});

/**
 * handle endpoint with folder
 */
config.routes(router, __filename, __dirname);

/**
 * handle endpoint with redirect to '/api
 */
router.all('/*', async (req, res) => {

  /**
   * send response with status not found
   */
  return r.errorNotFound(res);
});

module.exports = router;