const express = require('express');
const router = express.Router();

/**
 * handle endpoint with folder
 */
config.routes(router, __filename, __dirname);

module.exports = router;